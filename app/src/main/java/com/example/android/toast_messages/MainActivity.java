package com.example.android.toast_messages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       Button mSimpleBtn=findViewById(R.id.simple_btn);
       Button mCustomBtn=findViewById(R.id.custom_button);
        mSimpleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Simple Text Toast",Toast.LENGTH_LONG).show();
            }
        });
      mCustomBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
                Toast toast=new Toast(getApplicationContext());
                toast.setGravity(Gravity.CENTER_VERTICAL,0,0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(getLayoutInflater().inflate(R.layout.custom_toast,null));
                toast.show();
           }
        });

    }
}
